package com.brainbean.teamsync.service.impl;

import com.brainbean.teamsync.controller.TeamBean;
import com.brainbean.teamsync.controller.UserBean;
import com.brainbean.teamsync.schema.Lap;
import com.brainbean.teamsync.schema.Team;
import com.brainbean.teamsync.schema.TeamRepository;
import com.brainbean.teamsync.schema.User;
import com.brainbean.teamsync.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RaceServiceImpl implements RaceService {

    public static Specification<User> isRacer(User aUser) {
        return (racer, cq, cb) -> cb.equal(racer.get("racer"), Boolean.TRUE);
    }

    @Autowired
    TeamRepository itsTeamRepository;

    @Autowired
    TeamConverter itsTeamConverter;

    public List<TeamBean> getTeams(UserBean aUserBean) {
        return null;
    }

}
