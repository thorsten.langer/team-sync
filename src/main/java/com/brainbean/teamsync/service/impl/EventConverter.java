package com.brainbean.teamsync.service.impl;

import com.brainbean.teamsync.controller.EventBean;
import com.brainbean.teamsync.schema.Event;
import org.springframework.stereotype.Component;

@Component
public class EventConverter {

    public EventBean convert(Event aEvent) {
        EventBean eventBean = new EventBean();
        eventBean.setId(aEvent.getId());
        transferProperties(aEvent, eventBean);
        return eventBean;
    }

    public void transferProperties(Event aEvent, EventBean aEventBean) {
        aEventBean.setName(aEvent.getName());
    }

    public void transferProperties(EventBean aEventbean, Event aEvent) {
        aEvent.setName(aEventbean.getName());
    }
}
