package com.brainbean.teamsync.service.impl;

import com.brainbean.teamsync.controller.EventBean;
import com.brainbean.teamsync.controller.TeamBean;
import com.brainbean.teamsync.controller.UserBean;
import com.brainbean.teamsync.schema.Event;
import com.brainbean.teamsync.schema.Team;
import com.brainbean.teamsync.schema.User;
import com.brainbean.teamsync.schema.EventRepository;
import com.brainbean.teamsync.service.MasterDataService;
import com.brainbean.teamsync.schema.TeamRepository;
import com.brainbean.teamsync.schema.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MasterDataServiceImpl implements MasterDataService {

    // -- 

    @Autowired
    private UserRepository itsUserRepository;

    @Autowired
    private UserConverter itsUserConverter;

    @Transactional
    public UserBean createUser(UserBean aUserBean) {
        User user = new User();
        itsUserConverter.transferProperties(aUserBean, user);
        user = itsUserRepository.saveAndFlush(user);
        return itsUserConverter.convert(user);
    }

    @Override
    public UserBean getUser(Long aId) {
        User user = itsUserRepository.findById(aId).get();
        UserBean userBean = itsUserConverter.convert(user);
        return userBean;
    }

    @Override
    public List<UserBean> getUsers() {
        List<User> users = itsUserRepository.findAll();
        List<UserBean> collect = users.stream().map(itsUserConverter::convert).collect(Collectors.toList());
        return collect;
    }

    @Override
    @Transactional
    public UserBean updateUser(Long aId, UserBean aUserBean) {
        User user = itsUserRepository.findById(aId).get();
        itsUserConverter.transferProperties(aUserBean, user);
        return aUserBean;
    }

    @Override
    @Transactional
    public void deleteUser(Long aId) {
        itsUserRepository.deleteById(aId);
    }

    //

    @Autowired
    private EventRepository itsEventRepository;

    @Autowired
    private EventConverter itsEventConverter;

    @Transactional
    public EventBean createEvent(EventBean aEventBean) {
        Event event = new Event();
        itsEventConverter.transferProperties(aEventBean, event);
        event = itsEventRepository.saveAndFlush(event);
        return itsEventConverter.convert(event);
    }

    @Override
    public EventBean getEvent(Long aId) {
        Event event = itsEventRepository.findById(aId).get();
        EventBean eventBean = itsEventConverter.convert(event);
        return eventBean;
    }

    @Override
    public List<EventBean> getEvents() {
        List<Event> events = itsEventRepository.findAll();
        List<EventBean> collect = events.stream().map(itsEventConverter::convert).collect(Collectors.toList());
        return collect;
    }

    @Override
    @Transactional
    public EventBean updateEvent(Long aId, EventBean aEventBean) {
        Event event = itsEventRepository.findById(aId).get();
        itsEventConverter.transferProperties(aEventBean, event);
        return aEventBean;
    }

    @Override
    @Transactional
    public void deleteEvent(Long aId) {
        itsEventRepository.deleteById(aId);
    }


    //

    @Autowired
    private TeamRepository itsTeamRepository;

    @Autowired
    private TeamConverter itsTeamConverter;

    @Transactional
    public TeamBean createTeam(TeamBean aTeamBean) {
        Team team = new Team();
        itsTeamConverter.transferProperties(aTeamBean, team);
        team = itsTeamRepository.saveAndFlush(team);
        return itsTeamConverter.convert(team);
    }

    @Override
    public TeamBean getTeam(Long aId) {
        Team team = itsTeamRepository.findById(aId).get();
        TeamBean teamBean = itsTeamConverter.convert(team);
        return teamBean;
    }

    @Override
    public List<TeamBean> getTeams() {
        List<Team> teams = itsTeamRepository.findAll();
        List<TeamBean> collect = teams.stream().map(itsTeamConverter::convert).collect(Collectors.toList());
        return collect;
    }

    @Override
    @Transactional
    public TeamBean updateTeam(Long aId, TeamBean aTeamBean) {
        Team team = itsTeamRepository.findById(aId).get();
        itsTeamConverter.transferProperties(aTeamBean, team);
        return aTeamBean;
    }

    @Override
    @Transactional
    public void deleteTeam(Long aId) {
        itsTeamRepository.deleteById(aId);
    }
}
