package com.brainbean.teamsync.service.impl;

import com.brainbean.teamsync.controller.TeamBean;
import com.brainbean.teamsync.schema.Team;
import org.springframework.stereotype.Component;

@Component
public class TeamConverter {

    public TeamBean convert(Team aTeam) {
        TeamBean teamBean = new TeamBean();
        teamBean.setId(aTeam.getId());
        transferProperties(aTeam, teamBean);
        return teamBean;
    }

    public void transferProperties(Team aTeam, TeamBean aTeamBean) {
        aTeamBean.setName(aTeam.getName());
        aTeamBean.setFull(aTeam.isFull());
        aTeamBean.setMaxSize(aTeam.getMaxSize());
        aTeamBean.setShortName(aTeam.getShortName());
    }

    public void transferProperties(TeamBean aTeambean, Team aTeam) {
        aTeam.setName(aTeambean.getName());
    }

}
