package com.brainbean.teamsync.service.impl;

import com.brainbean.teamsync.controller.UserBean;
import com.brainbean.teamsync.schema.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public UserBean convert(User aUser) {
        UserBean userBean = new UserBean();
        userBean.setId(aUser.getId());
        transferProperties(aUser, userBean);
        return userBean;
    }

    public void transferProperties(User aUser, UserBean aUserBean) {
        aUserBean.setName(aUser.getName());
    }

    public void transferProperties(UserBean aUserbean, User aUser) {
        aUser.setName(aUserbean.getName());
    }
}
