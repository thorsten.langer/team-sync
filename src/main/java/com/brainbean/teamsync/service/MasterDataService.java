package com.brainbean.teamsync.service;

import com.brainbean.teamsync.controller.EventBean;
import com.brainbean.teamsync.controller.TeamBean;
import com.brainbean.teamsync.controller.UserBean;

import javax.transaction.Transactional;
import java.util.List;

public interface MasterDataService {

    UserBean createUser(UserBean aUserBean);

    List<UserBean> getUsers();

    UserBean updateUser(Long aId, UserBean aUserBean);

    UserBean getUser(Long aId);

    void deleteUser(Long aId);

    EventBean createEvent(EventBean aEventBean);

    EventBean getEvent(Long aId);

    List<EventBean> getEvents();

    EventBean updateEvent(Long aId, EventBean aEventBean);

    abstract void deleteEvent(Long aId);


    TeamBean createTeam(TeamBean aTeamBean);

    TeamBean getTeam(Long aId);

    List<TeamBean> getTeams();

    TeamBean updateTeam(Long aId, TeamBean aTeamBean);

    void deleteTeam(Long aId);
}
