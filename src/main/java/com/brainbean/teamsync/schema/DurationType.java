package com.brainbean.teamsync.schema;

public enum DurationType {

    /**
     * from official timing
     */
    OFFICIAL,

    /**
     * manually stopped
     */
    MANUAL,

    /**
     * estimated
     */
    ESTIMATED,

    /**
     *
     */
    USELESS,

}
