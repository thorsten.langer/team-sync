package com.brainbean.teamsync.schema;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class Team {

    @Id
    @GeneratedValue
    private Long id;

    private String shortName;

    private String name;

    private boolean recruiting;

    private boolean full;

    private int maxSize;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Event event;

}
