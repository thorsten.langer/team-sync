package com.brainbean.teamsync.schema;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Lap {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="membership_id", nullable = false)
    private MemberShip memberShip;

    /**
     * to be used for extrapolations
     */
    private boolean typical;

    /**
     * state
     */
    private LapState state;

    /**
     * duration
     */
    private Duration duration;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DurationType durationType;

    /**
     * begin
     */
    private LocalDateTime begin;

    /**
     * end
     */
    private LocalDateTime end;
}
