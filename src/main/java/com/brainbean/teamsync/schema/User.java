package com.brainbean.teamsync.schema;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String shortName;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private Long password;
}
