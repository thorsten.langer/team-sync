package com.brainbean.teamsync.schema;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class MemberShip {

    @Id
    @GeneratedValue
    private Long id;

    private String plateNumber;

    private boolean available;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name="team_id", nullable = false, updatable = false)
    private Team team;

    private boolean supporter;

    private boolean racer;

    private boolean guest;

}
