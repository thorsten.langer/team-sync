package com.brainbean.teamsync.schema;

public enum LapState {

    /**
     * geplante Runde.
     *
     * hat
     *  * entweder eine referenz auf eine vorherige Runde, oder eine Startzeit oder nichts davon
     *  * hat eine Membership oder auch nicht.
     *  * hat eine Duration oder auch nicht.
     */
    PLANNED,

    /**
     * wird aktuell gefahren
     */
    RUNNING,

    /**
     * verwerfen
     */
    CANCELED,

    /**
     * Runde ist abgeschlossen und kann auch gewertet werden.
     */
    FINISHED,

}
