package com.brainbean.teamsync.schema;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    private String shortName;

    private String name;

    private LocalDateTime begin;

    private LocalDateTime end;

}
