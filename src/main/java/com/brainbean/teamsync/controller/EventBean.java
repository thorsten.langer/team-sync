package com.brainbean.teamsync.controller;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

public class EventBean {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private LocalDateTime begin;

}
