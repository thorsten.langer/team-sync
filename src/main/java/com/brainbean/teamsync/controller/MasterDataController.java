package com.brainbean.teamsync.controller;

import com.brainbean.teamsync.service.MasterDataService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MasterDataController {

    MasterDataService itsMasterDataService;

    public MasterDataController(MasterDataService aMasterDataService) {
        itsMasterDataService = aMasterDataService;
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public List<UserBean> getAllUsers() {
        List<UserBean> users = itsMasterDataService.getUsers();
        return users;
    }

    @PostMapping(value = "user")
    public UserBean postUser(@RequestBody UserBean aUserBean) {
        UserBean userBean = itsMasterDataService.createUser(aUserBean);
        return userBean;
    }

    @PutMapping(value = "user/{id}")
    public UserBean putUser(@RequestBody UserBean aUserBean, @PathVariable("id") Long aId) {
        UserBean userBean = itsMasterDataService.updateUser(aId, aUserBean);
        return userBean;
    }

    @GetMapping(value = "user/{id}")
    public UserBean getUser(@PathVariable("id") Long aId) {
        UserBean userBean = itsMasterDataService.getUser(aId);
        return userBean;
    }

    @DeleteMapping(value = "user/{id}")
    public void deleteUser(@PathVariable("id") Long aId) {
        itsMasterDataService.deleteUser(aId);
    }

    // -----

    @GetMapping(value = "team")
    public List<TeamBean> getAllTeams() {
        List<TeamBean> teams = itsMasterDataService.getTeams();
        return teams;
    }

    @PostMapping(value = "team")
    public TeamBean postTeam(@RequestBody TeamBean aTeamBean) {
        TeamBean teamBean = itsMasterDataService.createTeam(aTeamBean);
        return teamBean;
    }

    @PutMapping(value = "team/{id}")
    public TeamBean putTeam(@RequestBody TeamBean aTeamBean, @PathVariable("id") Long aId) {
        TeamBean teamBean = itsMasterDataService.updateTeam(aId, aTeamBean);
        return teamBean;
    }

    @GetMapping(value = "team/{id}")
    public TeamBean getTeam(@PathVariable("id") Long aId) {
        TeamBean teamBean = itsMasterDataService.getTeam(aId);
        return teamBean;
    }

    @DeleteMapping(value = "team/{id}")
    public void deleteTeam(@PathVariable("id") Long aId) {
        itsMasterDataService.deleteTeam(aId);
    }

    // -----

    @DeleteMapping(value = "event")
    public List<EventBean> getAllEvents() {
        List<EventBean> events = itsMasterDataService.getEvents();
        return events;
    }

    @PostMapping(value = "event")
    public EventBean postEvent(@RequestBody EventBean aEventBean) {
        EventBean eventBean = itsMasterDataService.createEvent(aEventBean);
        return eventBean;
    }

    @PutMapping(value = "event/{id}")
    public EventBean putEvent(@RequestBody EventBean aEventBean, @PathVariable("id") Long aId) {
        EventBean eventBean = itsMasterDataService.updateEvent(aId, aEventBean);
        return eventBean;
    }

    @GetMapping(value = "event/{id}")
    public EventBean getEvent(@PathVariable("id") Long aId) {
        EventBean eventBean = itsMasterDataService.getEvent(aId);
        return eventBean;
    }

    @DeleteMapping(value = "event/{id}")
    public void deleteEvent(@PathVariable("id") Long aId) {
        itsMasterDataService.deleteEvent(aId);
    }
}
