package com.brainbean.teamsync.controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PrepareController {

    /**
     * Join a team.
     */
    @PostMapping(value = "team/{team_id}/user/{user_id}/join")
    public MemberShipBean joinTeam(
            @PathVariable("team_id") long aTeamId,
            @PathVariable("user_id") long aUserId,
            @RequestParam("plate") String aPlate
    ) {
        return null;
    }

    /**
     * Leave a team.
     */
    @DeleteMapping(value = "team/{team_id}/user/{user_id}")
    public void leaveTeam(@PathVariable("team_id") long aTeamId, @PathVariable("user_id") long aUserId) {
    }
    

}
