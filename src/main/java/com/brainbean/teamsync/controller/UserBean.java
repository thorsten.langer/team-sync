package com.brainbean.teamsync.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserBean {

    private Long id;

    private String shortName;

    private String name;

}
