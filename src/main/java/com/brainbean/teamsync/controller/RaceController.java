package com.brainbean.teamsync.controller;

import com.brainbean.teamsync.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class RaceController {

    @Autowired
    RaceService raceService;

    /**
     * eine Runde planen
     */
    @PostMapping(value = "team-plan/{team_id}/lap")
    public LapBean createLap(
            @PathVariable("team_id") long aTeamId, // Pflicht

            @RequestParam("user_id") Long aUserId, // Optional

            @RequestParam("begin_at") LocalDateTime aBeginAt, // Wahlpflicht
            @RequestParam("begin_after_lap") Long aLapId, // Wahlpflicht

            @RequestParam("duration") Duration aDuration, // Optional

            @RequestParam("typical") Boolean aTypical // Optional
    ) {
        return null;
    }

    /**
     * geplante Runde entfernen
     */
    @DeleteMapping(value = "team-plan/{team_id}/lap/{lap_id}")
    public void deleteLap(
            @PathVariable("team_id") long aTeamId,
            @PathVariable("lap_id") Long aLapId
    ) {
    }

    /**
     * geplante Runde ändern
     */
    @PutMapping(value = "team-plan/{team_id}/lap/{lap_id}")
    public LapBean updateLap(
            @PathVariable("team_id") Long aTeamId, // Pflicht
            @PathVariable("lap_id") Long aLapId, // Pflicht

            @RequestParam("user_id") Long aUserId, // Optional

            @RequestParam("begin_at") LocalDateTime aBeginAt, // Optionale Wahlpflicht
            @RequestParam("begin_after_lap") Long aBeginAfterLapId,

            @RequestParam("end_at") LocalDateTime aEnd, // Wahlpflicht

            @RequestParam("duration") Duration aDuration, // Optional

            @RequestParam("typical") Boolean aTypical // Optional
    ) {
        return null;
    }

    /**
     * geplante Runde ändern
     */
    @GetMapping(value = "team-plan/{team_id}/lap")
    public List<LapBean> getTeamPlan(
            @PathVariable("team_id") Long aTeamId) {
        return null;
    }
}
