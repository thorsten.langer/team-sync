package com.brainbean.teamsync.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamBean {

    private Long id;

    private String name;

    private String shortName;

    private boolean recruiting;

    private boolean full;

    private int maxSize;

    private Long eventId;

}
