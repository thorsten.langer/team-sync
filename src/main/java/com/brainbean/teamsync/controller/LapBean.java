package com.brainbean.teamsync.controller;

import com.brainbean.teamsync.schema.LapState;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Setter
public class LapBean {

    private Long id;

    private Long eventId;

    private Long userId;

    private Long userName;

    private boolean typical;

    private LapState state;

    private Duration duration;

    private LocalDateTime begin;

    private LocalDateTime end;

}
