package com.brainbean.teamsync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class TeamsyncServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeamsyncServerApplication.class, args);
	}

}
