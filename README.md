Tool to document and plan laps in a lap bicycle race.
===

Typical Usecases (questions):
 
 * Very most important: When (time) will one racer start the next time?
 * How many laps will we make total and individually?
 * Who is in the track currently?
 * Who will be the racer before and after me?
